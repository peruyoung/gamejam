﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public float horizontalMove;
    public float verticalMove;
    private Vector3 playerInput;
    public CharacterController player;
    public GameObject pj;
    public float playerSpeed;
    private Vector3 movePlayer;
    public float gravity = 9.8f;
    public float fallVelocity;
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    public int disparo = 1;
    private float timer;
    public bool moverRepair = false;

    //Variables de sonido
    public AudioSource audioData;
    public AudioClip[] audioLibrary;
    private float timerstep;
    bool isplayingstep = false;

    //Variables de animación
    public Animator anim;
    public GameObject tool;
    public bool equipment = true;
    public GameObject levelmanager;

    // Start is called before the first frame update
    void Start()
    {
        //Componentes del GameObject
        player = GetComponent<CharacterController>();
        anim = pj.GetComponent<Animator>();

        //Componentes del sonido
        audioData = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {

        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        //Temporizador del sonido de los pasos
        if(Time.time - timerstep > 0.4f)
        {
            efectSound(horizontalMove,verticalMove);
        }
        if(player.velocity.magnitude  < 1)
        {
            audioData.Stop();
        }
        anim.SetFloat("Speed", player.velocity.magnitude);
        playerInput = new Vector3(horizontalMove, 0, verticalMove);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);
        camDirection();
        movePlayer = playerInput.x * camRight + playerInput.z * camForward;
        // playerSpeed  invertir controles
        movePlayer = movePlayer * playerSpeed * disparo;
        player.transform.LookAt(player.transform.position + movePlayer);
        setGravity();
        player.Move(movePlayer * Time.deltaTime);
        if (moverRepair == true)
        {
            playerSpeed = 0;
        }
        else
        {
            playerSpeed = 30;
        }
        //Gravedad
        /*if (moverRepair == false)
        {
            player.transform.LookAt(player.transform.position + movePlayer);
            //Gravedad
            setGravity();
            player.Move(movePlayer * Time.deltaTime);
        }*/
        //player.Move(new Vector3(horizontalMove,0,verticalMove) * playerSpeed * Time.deltaTime);
        //Debug.Log(player.velocity.magnitude);
        if (disparo == -1 && Time.time - timer > 5)
        {
            disparo = 1;
        }
    }
    //vision movimiento de camara
    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;
        camForward.y = 0;
        camRight.y = 0;
        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    //Randomizador de pasos
    void efectSound(float h, float v)
    {
        if ((h != 0 || v != 0) && (!isplayingstep))
        {
            timerstep = Time.time;
            audioData.clip = audioLibrary[Random.Range(0, audioLibrary.Length)];
            audioData.Play();
        }
    }

    //Set de gravedad
    void setGravity()
    {
        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
    }

    //Invertir controles
    public void setInvert()
    {
        disparo = -1;
        timer = Time.time;
    }

    //Perdida de la herramienta
    public void LoseTool()
    {
        if (equipment == true)
        {
        anim.SetBool("Equiped", false);
        equipment = false;
        tool.SetActive(false);
        levelmanager.GetComponent<LevelManager>().PlaceTool();
        }
    }

    //Equipado de herramienta
    public void setTool()
    {
        anim.SetBool("Equiped", true);
        equipment = true;
        tool.SetActive(true);
        levelmanager.GetComponent<LevelManager>().Scoreupdate(1);
    }
}