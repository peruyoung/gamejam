﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableObject : MonoBehaviour
{
    //Variables
    public bool isPickable = true;

    //Dentro de radio de acción asignar objeto
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerInteraction")
        {
            other.GetComponentInParent<PickUpObjects>().ObjectToPickUp = this.gameObject;
        }
    }

    //Cambiar objeto objetivo al alejarse
    private void OnTriggerExit(Collider other) 
    {
        if (this.gameObject.GetComponent<PlayerController>().moverRepair == false && other != null && other.tag == "PlayerInteraction" )
        {
            other.GetComponentInParent<PickUpObjects>().ObjectToPickUp = null;
        }
    }
}