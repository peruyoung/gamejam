﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    //Variables del respawner
    public GameObject[] enemies;
    public GameObject[] respawn_points;
    private float timer;
    public float t_respawn;
    public GameObject tool;

    //Variables HUD
    int contador;
    public Text score;
    private float minutes;
    private float seconds;
    private float time_end;
    private float time_left;
    public Text countdown;
    private bool fin_tiempo = false;
    public Text over;
    public int deadTime;

    // Awake is called only at the beginning
    void Awake()
    {
        contador = 0;
        time_end = Time.time + deadTime; // Modificar con la cantidad de minutos de juego
        Scoreupdate(0); // Incluir en el método donde se consiguen puntos esta línea y antes una línea con "contador = contador + 1"
        countdown.text = "Timer: " + Mathf.Floor(deadTime / 60) + ":" + Mathf.RoundToInt(deadTime % 60);
        over.enabled = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        respawn_points = GameObject.FindGameObjectsWithTag("Respawn");
        timer = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //Instanciado de elementos
        if (Time.time - timer > t_respawn)
        {
            //Debug.Log(this.gameObject.transform.position);
            Instantiate(enemies[Random.Range(0, enemies.Length)], respawn_points[Random.Range(0, respawn_points.Length)].transform.position, gameObject.transform.rotation);
            timer = Time.time;
        }

        //Actualizador de temporizador
        time_left = time_end - Time.time;
        if (time_left <= 0)
        {
            time_left = 0;
            fin_tiempo = true;
            GameOver(); // Función para el gameover
        }
        if (!fin_tiempo)
        {
            minutes = Mathf.Floor(time_left / 60);
            seconds = time_left % 60;
            countdown.text = "Timer: " + minutes + ":" + Mathf.RoundToInt(seconds);
        }
    }

    //Método actualizacion marcador
    public void Scoreupdate(int points)
    {
        contador += points;
        score.text = "Puntuación: " + contador;
    }
    //Función de gameover
    void GameOver()
    {
        over.enabled = true;
        Time.timeScale = 0f;
    }

    public void PlaceTool ()
    {
        Instantiate(tool, respawn_points[Random.Range(0, respawn_points.Length)].transform.position, gameObject.transform.rotation);
    }
}
