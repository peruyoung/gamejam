﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObjects : MonoBehaviour
{
    public GameObject ObjectToPickUp;
    public GameObject PickedObject;
    public Transform interactionZone;

    // Coger objetos
    void Update()
    {
        if (ObjectToPickUp != null && ObjectToPickUp.GetComponent<PickableObject>().isPickable == true && PickedObject == null)
        {
            if (Input.GetKeyDown(KeyCode.Q) && PickedObject == null && ObjectToPickUp != null && this.gameObject.GetComponent<PlayerController>().equipment == true)
            {
                //Asignar Objeto a reparar
                PickedObject = ObjectToPickUp;   
                FixingTime();
            }
        }
        else if (ObjectToPickUp != null && ObjectToPickUp.transform.parent.GetComponent<MoveRandomly>().moverRepair == true)
        {
            if (Input.GetKeyUp(KeyCode.Q) && PickedObject != null)
            {
                EndFixing();
            }
        }

        /*if (PickedObject != null)
        {
            if(Input.GetKeyUp(KeyCode.Q) && ObjectToPickUp != null)
            {
                GetComponent<MoveRandomly>().moverRepair = false;
                GetComponent<PlayerController>().moverRepair = false;
            }
        }*/
    }

    //Modo reparacion
    public void FixingTime ()
    {
        this.gameObject.GetComponent<PlayerController>().anim.SetBool("Fixing", true);
        ObjectToPickUp.transform.parent.GetComponent<MoveRandomly>().moverRepair = true;
        GetComponent<PlayerController>().moverRepair = true;
    }

    //Dejar de reparar
    public void EndFixing ()
    {
        this.gameObject.GetComponent<PlayerController>().anim.SetBool("Fixing", false);
        ObjectToPickUp.transform.parent.GetComponent<MoveRandomly>().moverRepair = false;
        GetComponent<PlayerController>().moverRepair = false;
        PickedObject = null;
    }
}