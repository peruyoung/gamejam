﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class EnemiesController : MonoBehaviour
{
    //Variables públicas
    public float agroDistance;
    public float patrolDistance;

    //Variables privadas
    private GameObject player;
    private NavMeshAgent agent;
    private GameObject[] patrolPoint;
    private Transform actualPatrol;
    private PlayerController playerScript;

    //Inicialización
     void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindWithTag("Player");
        patrolPoint = GameObject.FindGameObjectsWithTag("PatrolPoint");
        playerScript = player.GetComponent<PlayerController>();
        //Inicializa la patrulla
        NextPatrol();
    }

    //Update
    void FixedUpdate()
    {
        //Patrulla
        if (Vector3.Distance(this.transform.position, player.transform.position) > agroDistance)
        {
            //Debug.Log("Patrulla");
            if (Vector3.Distance(this.transform.position, actualPatrol.position) < patrolDistance)
            {
                //Debug.Log("Cambio de patrulla");
                NextPatrol();
            }
            else
            {
                agent.SetDestination(actualPatrol.position);
            }
        }
        //Detección del jugador
        else
        {
            agent.SetDestination(player.transform.position); 
        }
    }

    //Colisiones
    private void OnCollisionEnter(Collision other)
    {
        //Collisión contra el Player
        if (other.gameObject.tag == "Player")
        {
            if (this.gameObject.tag == "Slime")
            {
                //Hace soltar la herramienta al Player si es tag "Slime"
                playerScript.LoseTool();
                if (playerScript.moverRepair == true)
                {
                    player.GetComponent<PickUpObjects>().EndFixing();
                }
            } else if (this.gameObject.tag == "Dust")
            {
                //Invierte los controles al Player si es tag "Dust"
                playerScript.setInvert();
                if (playerScript.moverRepair == true)
                {
                    player.GetComponent<PickUpObjects>().EndFixing();
                }
            }
            //Destrucción del GameObject contra el Player
            Destroy(this.gameObject);
        }
    }

    //Elección de punto de patrulla
    void NextPatrol ()
    {
        actualPatrol = patrolPoint[Random.Range(0, patrolPoint.Length)].transform;
    }

    ///Destrucción del personaje
    public void Destroyer ()
    {
        Destroy(this.gameObject);
    }
}
