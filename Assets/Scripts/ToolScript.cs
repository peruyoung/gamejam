﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolScript : MonoBehaviour
{
    void Update()
    {
        this.gameObject.transform.Rotate(0, 1, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().setTool();
            Destroy (this.gameObject);
        }
    }
}
