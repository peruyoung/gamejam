﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveRandomly : MonoBehaviour
{


    public float speed = 5.0f;
    private bool is_posible_collision = true;
    private bool is_posible_move = true;
    private Vector3 moveDirection = new Vector3(0, 0f, 0);
    private CharacterController characterController;
    private float currentTime = 0;
    private float timeToStopRumba = 0;
    public float gravity = 9.8f;
    public float fallVelocity;
    private Vector3 movePlayer;
    public GameObject hoover;
    public bool moverRepair = false;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        moveDirection.z = 1;
    }

    /*void Update()
    {
        currentTime += 1 * Time.deltaTime;
        Debug.Log(currentTime);
        if (is_posible_move)
        {
            moveDirection.y = 0f;  
            characterController.Move(moveDirection * Time.deltaTime * speed);
        } else {
            active_moved();
        }
    }*/

    private void FixedUpdate()
    {
        currentTime += 1 * Time.deltaTime;
        //Debug.Log(currentTime);
        if (moverRepair == true)
        {
            speed = 0;
        }
        else
        {
            speed = 20;
        }
        if (is_posible_move)
        {
            moveDirection.y = 0f;  
            characterController.Move(moveDirection * Time.deltaTime * speed);
        } else {
            active_moved();
        }
        setGravity();
    }
    
    void OnControllerColliderHit(ControllerColliderHit hit) {
        if (hit.collider.tag == "Wall") {
            if (is_posible_collision) {
                transform.Rotate(0, Random.Range(-180, 180), 0, Space.Self);
                
                moveDirection = transform.rotation.eulerAngles;
                float random = Random.Range(-1f, 1f);
                while (random == 0f)
                {
                    random = Random.Range(-1f, 1f);
                }
                moveDirection.x = Mathf.Cos(transform.rotation.y) * random * 1.3f;
                moveDirection.z = Mathf.Sin(transform.rotation.y) * random * 1.3f;
                moveDirection.y = 0f;             
            }
        }
    }

    private void active_moved() {
        if (currentTime >= timeToStopRumba)
        {
            is_posible_move = true;
            is_posible_collision = true;
        }
    }

    public void StopRumbaMoved(float timeStop = 1) {
        is_posible_collision = false;
        is_posible_move = false;
        LevelManager lm = this.gameObject.GetComponent<LevelManager>();
        lm.Scoreupdate(1);
        currentTime = 0;
        timeToStopRumba = timeStop;
        // DestroyScriptIstance();
        // Destroy(gameObject);
    }

    void setGravity()
    {
        if (characterController.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
    }

    void DestroyScriptIstance() {
        Destroy(this);
    }
}
